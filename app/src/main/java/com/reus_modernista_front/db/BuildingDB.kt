package com.reus_modernista_front.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.reus_modernista_front.db.entity.BuildingEntity

@Database(entities = [BuildingEntity::class], version = 3, exportSchema = true)
abstract class BuildingDB : RoomDatabase() {

    abstract fun buildingDAO(): BuildingDAO

    companion object {

        @Volatile //https://www.baeldung.com/kotlin/volatile-properties -> para entender porque hacer esto

        private var INSTANCE: BuildingDB? = null

        //Mirarlo más detenidamente este bloque
        fun getDataBase(context: Context): BuildingDB {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) { // Para que solo un thread pueda ejecutar esto
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BuildingDB::class.java,
                    "building_data"
                ).fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }

        }
    }

}