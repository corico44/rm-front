package com.reus_modernista_front.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "building_data")
data class BuildingEntity(
    @ColumnInfo(name = "_id") val _id: String,
    @PrimaryKey val name: String,
    @ColumnInfo(name = "year_of_creation") val year_of_creation: String,
    @ColumnInfo(name = "architect") val architect: String,
    @ColumnInfo(name = "latitude") val latitude: Double,
    @ColumnInfo(name = "longitude") val longitude: Double,
    @ColumnInfo(name = "value") val value: String
)