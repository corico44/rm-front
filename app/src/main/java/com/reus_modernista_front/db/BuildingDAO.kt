package com.reus_modernista_front.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.reus_modernista_front.db.entity.BuildingEntity

@Dao
interface BuildingDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addBuilding(buildingEntity: BuildingEntity)

    @Query("SELECT * FROM building_data")
    suspend fun getAllBuildings(): List<BuildingEntity>
}