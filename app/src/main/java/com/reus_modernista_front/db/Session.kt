package com.reus_modernista_front.db

class Session {
    companion object {

        private var email = ""
        private var isGoogleAccount = false

        fun loginUserLocalDB(email: String, isGoogleAccount: Boolean) {
            this.email = email
            this.isGoogleAccount = isGoogleAccount
        }

        fun getEmailUsuario(): String {
            return email
        }

        fun getIsGoogleAccount(): Boolean {
            return isGoogleAccount
        }
    }
}