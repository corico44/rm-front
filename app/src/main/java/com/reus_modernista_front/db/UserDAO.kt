package com.reus_modernista_front.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.reus_modernista_front.db.entity.UserEntity

@Dao
interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUser(userEntity: UserEntity)

    @Query("SELECT * FROM user_data WHERE email = :email LIMIT 1")
    fun readDataLoggedUser(email: String): LiveData<UserEntity>

    @Query("SELECT EXISTS(SELECT * FROM user_data WHERE email = :email and password = :password)")
    suspend fun getUserInfo(email: String, password: String): Boolean

    @Query("SELECT EXISTS(SELECT * FROM user_data WHERE email = :email)")
    suspend fun getUserInfoByEmail(email: String): Boolean

    @Query("UPDATE user_data SET points = :points WHERE email = :email")
    suspend fun addPoints(points: Int, email: String)

    @Query("SELECT points FROM user_data WHERE email = :email")
    suspend fun getPoints(email: String): Int

    @Query("UPDATE user_data SET visited = :visitedBuilding WHERE email = :email")
    suspend fun addVisitedBuilding(email: String, visitedBuilding: String)

    @Query("SELECT visited FROM user_data WHERE email = :email")
    suspend fun getVisitedBuilding(email: String): String
}