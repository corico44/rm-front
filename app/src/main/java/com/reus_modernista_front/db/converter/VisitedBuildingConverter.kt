package com.reus_modernista_front.db.converter

import androidx.room.TypeConverter

class VisitedBuildingConverter {

    @TypeConverter
    fun storedStringToVisitedBuildings(value: String?): MutableList<String>? {
        if (value != null) {
            return value.split(",").toMutableList()
        }
        return ArrayList()
    }

    @TypeConverter
    fun VisitedBuildingsToStoredString(cl: MutableList<String>?): String? {
        var loopSize = 0
        var value = ""
        if (cl != null) {
            for (building in cl) {
                value += "$building"
                ++loopSize
                if (loopSize + 1 <= cl.size) value += ","
            }
        }
        return value
    }
}