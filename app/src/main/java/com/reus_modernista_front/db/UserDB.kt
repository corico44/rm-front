package com.reus_modernista_front.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.reus_modernista_front.db.converter.VisitedBuildingConverter
import com.reus_modernista_front.db.entity.UserEntity

@Database(entities = [UserEntity::class], version = 4, exportSchema = true)
@TypeConverters(VisitedBuildingConverter::class)
abstract class UserDB : RoomDatabase() {

    abstract fun userDAO(): UserDAO

    companion object {

        @Volatile //https://www.baeldung.com/kotlin/volatile-properties -> para entender porque hacer esto

        private var INSTANCE: UserDB? = null

        //Mirarlo más detenidamente este bloque
        fun getDataBase(context: Context): UserDB {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) { // Para que solo un thread pueda ejecutar esto
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    UserDB::class.java,
                    "user_data"
                ).fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }

        }
    }

}