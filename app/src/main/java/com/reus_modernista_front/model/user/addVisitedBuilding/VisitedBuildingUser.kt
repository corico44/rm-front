package com.reus_modernista_front.model.user.addVisitedBuilding

data class VisitedBuildingUser(
    val email: String,
    val visitedBuildingName: String
)