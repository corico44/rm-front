package com.reus_modernista_front.model.user.authUser.login

import com.reus_modernista_front.model.user.authUser.UserRequest

class LoginUserRequest(
    email: String,
    password: String
) : UserRequest(email, password)