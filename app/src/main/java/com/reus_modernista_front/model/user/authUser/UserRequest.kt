package com.reus_modernista_front.model.user.authUser

open class UserRequest(
    val email: String,
    val password: String
)