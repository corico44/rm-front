package com.reus_modernista_front.model.user.getInfo

data class GetInfoUserResponse(
    val name: String,
    val email: String,
    val password: String,
    val points: Int,
    val visited: MutableList<String>?
)