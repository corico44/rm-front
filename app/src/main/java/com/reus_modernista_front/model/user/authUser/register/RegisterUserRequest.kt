package com.reus_modernista_front.model.user.authUser.register

import com.reus_modernista_front.model.user.authUser.UserRequest

class RegisterUserRequest(
    val name: String,
    email: String,
    password: String
) : UserRequest(email, password)