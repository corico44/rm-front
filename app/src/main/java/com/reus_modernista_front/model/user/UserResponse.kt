package com.reus_modernista_front.model.user

data class UserResponse(
    val code: String,
    val message: String
)