package com.reus_modernista_front.model.user.getRanking

data class GetRankingUserResponse(
    val name: String,
    val points: Int
)