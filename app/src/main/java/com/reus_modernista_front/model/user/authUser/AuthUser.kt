package com.reus_modernista_front.model.user.authUser

import android.text.TextUtils

open class AuthUser(
    val email: String,
    val password: String,
    val isEmailValid: Boolean,
    val isPasswordValid: Boolean
) {

    fun isEmailValid(email: String): Boolean {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isPasswordValid(password: String): Boolean {
        return !TextUtils.isEmpty(password) && password.length > 5
    }

}