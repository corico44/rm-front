package com.reus_modernista_front.model.user.authUser.register

import android.text.TextUtils
import com.reus_modernista_front.model.user.authUser.AuthUser

class RegisterAuthUser(
    val name: String,
    email: String,
    password: String,
    val isNameValid: Boolean,
    isEmailValid: Boolean,
    isPasswordValid: Boolean
) : AuthUser(
    email,
    password,
    isEmailValid,
    isPasswordValid
) {

    fun isNameValid(name: String): Boolean {
        return !TextUtils.isEmpty(name)
    }

}