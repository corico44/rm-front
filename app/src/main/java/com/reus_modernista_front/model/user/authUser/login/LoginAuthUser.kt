package com.reus_modernista_front.model.user.authUser.login

import com.reus_modernista_front.model.user.authUser.AuthUser

class LoginAuthUser(
    email: String,
    password: String,
    isEmailValid: Boolean,
    isPasswordValid: Boolean
) : AuthUser(
    email,
    password,
    isEmailValid,
    isPasswordValid
)