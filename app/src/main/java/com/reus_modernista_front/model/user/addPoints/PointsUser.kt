package com.reus_modernista_front.model.user.addPoints

data class PointsUser(
    val email: String,
    val points: Int
)