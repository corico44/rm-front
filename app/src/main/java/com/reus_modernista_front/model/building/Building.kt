package com.reus_modernista_front.model.building

data class Building(
    val _id: String,
    val name: String,
    val year_of_creation: String,
    val architect: String,
    val latitude: Double,
    val longitude: Double,
    val value: String
)