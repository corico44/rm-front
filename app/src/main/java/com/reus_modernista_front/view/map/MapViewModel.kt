package com.reus_modernista_front.view.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.reus_modernista_front.db.BuildingDAO
import com.reus_modernista_front.db.Session
import com.reus_modernista_front.db.UserDAO
import com.reus_modernista_front.model.building.Building
import com.reus_modernista_front.model.user.addPoints.PointsUser
import com.reus_modernista_front.model.user.addVisitedBuilding.VisitedBuildingUser
import com.reus_modernista_front.repository.BuildingRepository
import com.reus_modernista_front.repository.UserRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class MapViewModel(userDAO: UserDAO, buildingDAO: BuildingDAO) : ViewModel() {

    val response = MutableLiveData<List<Building>>()


    private val buildingRepository = BuildingRepository(buildingDAO)
    private val userRepository = UserRepository(userDAO)


    fun getBuildings() {
        viewModelScope.launch {
            val localBuildings = buildingRepository.getLocalBuildings()
            if (localBuildings != null) {
                val visitedBuildings: MutableList<String> =
                    getVisitedBuilding(Session.getEmailUsuario()).split(",").toMutableList()
                for (building in visitedBuildings) {
                    with(localBuildings.listIterator()) {
                        forEach {
                            if (it.name == building) {
                                remove()
                            }
                        }
                    }
                }
                response.value = localBuildings
            } else {
                val res: Response<List<Building>>? = buildingRepository.getBuildings()
                if (res!!.isSuccessful) {
                    for (building in res.body()!!) {
                        val build = com.reus_modernista_front.db.entity.BuildingEntity(
                            building._id,
                            building.name,
                            building.year_of_creation,
                            building.architect,
                            building.latitude,
                            building.longitude,
                            building.value
                        )
                        buildingRepository.addBuilding(build)
                    }
                }
                val visitedBuildings: MutableList<String> =
                    getVisitedBuilding(Session.getEmailUsuario()).split(",").toMutableList()
                val buildings: MutableList<Building>? = res!!.body() as MutableList<Building>?
                for (building in visitedBuildings) {
                    if (buildings != null) {
                        with(buildings.listIterator()) {
                            forEach {
                                if (it.name == building) {
                                    remove()
                                }
                            }
                        }
                    }
                }
                response.value = buildings
            }
        }
    }

    fun addPoints(pointsUser: PointsUser) {
        viewModelScope.launch {
            userRepository.addLocalPoints(pointsUser.points)
            userRepository.addPoints(pointsUser)
        }
    }

    fun addVisitedBuilding(visitedBuildingUser: VisitedBuildingUser) {
        viewModelScope.launch {
            userRepository.addVisitedBuilding(visitedBuildingUser)
            var visitedBuildings = getVisitedBuilding(visitedBuildingUser.email)
            if (visitedBuildings.isEmpty()) visitedBuildings += visitedBuildingUser.visitedBuildingName
            else visitedBuildings += "," + visitedBuildingUser.visitedBuildingName
            userRepository.addLocalVisitedBuilding(visitedBuildingUser.email, visitedBuildings)
        }
    }

    private suspend fun getVisitedBuilding(email: String): String {
        return userRepository.getLocalVisitedBuilding(email)
    }

}