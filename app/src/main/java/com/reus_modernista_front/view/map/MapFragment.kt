package com.reus_modernista_front.view.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.content.res.Resources
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapClickListener
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.reus_modernista_front.R
import com.reus_modernista_front.databinding.FragmentMapBinding
import com.reus_modernista_front.db.BuildingDB
import com.reus_modernista_front.db.Session
import com.reus_modernista_front.db.UserDB
import com.reus_modernista_front.model.building.Building
import com.reus_modernista_front.model.user.addPoints.PointsUser
import com.reus_modernista_front.model.user.addVisitedBuilding.VisitedBuildingUser
import com.reus_modernista_front.view.buildingInfo.BuildingInfoActivity
import java.text.Normalizer
import java.util.concurrent.TimeUnit


class MapFragment : Fragment(), OnMapReadyCallback, OnMarkerClickListener, OnMapClickListener {

    private lateinit var binding: FragmentMapBinding
    private lateinit var currentLocation: Location
    private lateinit var supportMapFragment: SupportMapFragment
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var googleMap: GoogleMap

    private lateinit var mapViewModel: MapViewModel

    private lateinit var buildings: MutableList<Building>
    private lateinit var currentBuilding: Building
    private var buildingMapInfo: MutableList<Triple<Marker, Building, Circle>> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val userDAO = UserDB.getDataBase(activity?.application!!).userDAO()
        val buildingDAO = BuildingDB.getDataBase(activity?.application!!).buildingDAO()
        mapViewModel = MapViewModel(userDAO, buildingDAO)

        binding = FragmentMapBinding.inflate(inflater, container, false)

        binding.backgroundBuilding.isVisible = false
        binding.buildingName.isVisible = false
        binding.buildingInfo.isVisible = false
        binding.buildingImage.isVisible = false
        binding.botonAccedeEdificio.isVisible = false
        binding.greyBackground.isVisible = false

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult.equals(null)) {
                    return
                }
                for (location in locationResult.locations) {
                    currentLocation = location
                    focusOnMyLocation()
                    Log.d(TAG, "onLocationResult: $location")
                }
            }
        }

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize map fragment
        supportMapFragment = (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?)!!
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!)

        locationRequest = LocationRequest.create().apply {
            interval = TimeUnit.SECONDS.toMillis(60)
            fastestInterval = TimeUnit.SECONDS.toMillis(30)
            maxWaitTime = TimeUnit.MINUTES.toMillis(2)
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        mapViewModel.response.observe(this) {
            buildings = it as MutableList<Building>
            addBuildingsToMap()
        }

        binding.botonAccedeEdificio.setOnClickListener() {
            if ("low" == currentBuilding.value) {
                val pointsUser = PointsUser(Session.getEmailUsuario(), 100)
                mapViewModel.addPoints(pointsUser)
                buildings.remove(currentBuilding)
                val visitedBuildingUser = VisitedBuildingUser(Session.getEmailUsuario(), currentBuilding.name)
                mapViewModel.addVisitedBuilding(visitedBuildingUser)

                addBuildingsToMap()

                invisbleAllComponents()
                onButtonShowPopupWindowClick(100)
            } else {
                //Why send info and not have models with info? like session like currentbuilding
                val intent = Intent(activity!!, BuildingInfoActivity::class.java)
                intent.putExtra("buildingName", binding.buildingName.text)
                intent.putExtra("buildingArchitect", binding.buildingInfo.text.split(" - ")[0])
                startActivity(intent)
                activity!!.finish()
            }
        }

        fetchLocation()

    }

    private fun addBuildingsToMap() {
        if (buildingMapInfo.isNotEmpty()) {
            googleMap.clear()
            buildingMapInfo.clear()
        }
        for (building in buildings) {
            val bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.small_pin)
            val marker = googleMap.addMarker(
                MarkerOptions().position(LatLng(building.latitude, building.longitude)).icon(bitmapDescriptor)
            )!!

            if ("low" == building.value) {
                val circle = googleMap.addCircle(
                    CircleOptions()
                        .center(LatLng(building.latitude, building.longitude))
                        .radius(20.0)
                        .fillColor(0x6062BCFF)
                        .strokeColor(0x6062BCFF)
                        .strokeWidth(2f)
                )
                buildingMapInfo.add(Triple(marker, building, circle))
            } else if ("mid" == building.value) {
                val circle = googleMap.addCircle(
                    CircleOptions()
                        .center(LatLng(building.latitude, building.longitude))
                        .radius(20.0)
                        .fillColor(0x60ffbd38)
                        .strokeColor(0x60ffbd38)
                        .strokeWidth(2f)
                )
                buildingMapInfo.add(Triple(marker, building, circle))
            } else if ("high" == building.value) {
                val circle = googleMap.addCircle(
                    CircleOptions()
                        .center(LatLng(building.latitude, building.longitude))
                        .radius(20.0)
                        .fillColor(0x60fc5168)
                        .strokeColor(0x60fc5168)
                        .strokeWidth(2f)
                )
                buildingMapInfo.add(Triple(marker, building, circle))
            }
        }
        val extras = activity!!.intent.extras
        if (extras != null) {
            //TODO: NO FUNCIONA EL POPUP
            val points = extras.getString("points")?.toInt()
            if (points != null) {
                //SI TE VAS DE LA PANTALLA Y VUELVES, VUELVE A SALIR! NO SE ELIMINA DE VERDAD
                onButtonShowPopupWindowClick(points)
                //extras.remove("points")
                activity!!.intent.removeExtra("points")
            }
        }

    }

    private fun currentLocationIsInsideBuilding(marker: Circle): Boolean {
        val distance = FloatArray(3)
        val currentLatitude = currentLocation.latitude
        val currentLongitude = currentLocation.longitude
        val circleLatitude = marker.center.latitude
        val circleLongitude = marker.center.longitude
        Location.distanceBetween(
            currentLatitude, currentLongitude,
            circleLatitude, circleLongitude, distance
        )
        if (distance[0] <= marker.radius) {
            return true
        }
        return false
    }

    @SuppressLint("SetTextI18n")
    override fun onMarkerClick(currentMarker: Marker): Boolean {
        for (marker in buildingMapInfo) {
            if (currentMarker == marker.first) {
                currentBuilding = marker.second
                binding.backgroundBuilding.isVisible = true
                binding.buildingName.text = marker.second.name
                binding.buildingName.isVisible = true
                binding.buildingInfo.text = marker.second.architect + " - " + marker.second.year_of_creation
                binding.buildingInfo.isVisible = true
                binding.buildingImage.isVisible = true
                binding.greyBackground.isVisible = true

                var imageName = Normalizer.normalize(marker.second.name, Normalizer.Form.NFD)
                imageName = Regex("\\p{InCombiningDiacriticalMarks}+").replace(imageName, "")

                imageName = imageName.replace(" ", "_").lowercase()
                binding.buildingImage.setImageResource(
                    resources.getIdentifier(
                        imageName,
                        "drawable",
                        activity!!.packageName
                    )
                )

                binding.botonAccedeEdificio.isVisible = false
                if ((currentLocationIsInsideBuilding(marker.third))) {
                    binding.botonAccedeEdificio.isVisible = true
                }

                if ("low" == currentBuilding.value) {
                    binding.buildingDetail.text = "¡Visita el lugar y obtén 100 puntos!"
                    binding.botonAccedeEdificio.text = "RECOGE PUNTOS"
                } else if ("mid" == currentBuilding.value) {
                    binding.buildingDetail.text =
                        "¡Visita el lugar, responde preguntas y obtén hasta 300 puntos!\nRECUERDA: Cuanto más rápido seas, más puntos obtendrás"
                    binding.botonAccedeEdificio.text = "¡A JUGAR!"
                } else if ("high" == currentBuilding.value) {
                    binding.buildingDetail.text =
                        "¡Visita el lugar, responde preguntas y obtén hasta 400 puntos!\nRECUERDA: Cuanto más rápido seas, más puntos obtendrás"
                    binding.botonAccedeEdificio.text = "¡A JUGAR!"
                }
                return true
            }
        }

        return false
    }


    private fun fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                activity!!, Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity!!, Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity!!,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                1
            )
            Toast.makeText(activity,"¡Activa el permiso de ubicación para disfrutar de la aplicación!",Toast.LENGTH_SHORT).show();
            return
        }
        val task = fusedLocationProviderClient.lastLocation
        task.addOnSuccessListener { location ->
            if (location != null) {
                currentLocation = location
                Log.d(TAG, "${currentLocation.latitude}  ${currentLocation.longitude}")
                supportMapFragment.getMapAsync(this)
            }
        }

    }

    fun focusOnMyLocation() {
        val latLng = LatLng(currentLocation.latitude, currentLocation.longitude);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17F));
    }

    override fun onMapReady(mGoogleMap: GoogleMap) {
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        googleMap = mGoogleMap

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            val success: Boolean = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    activity!!, R.raw.map_style
                )
            )
            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e("MapsActivityRaw", "Can't find style.", e)
        }

        googleMap.isMyLocationEnabled = true
        val latLng = LatLng(currentLocation.latitude, currentLocation.longitude);
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17F));
        getBuildings()
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMapClickListener(this)
    }

    private fun getBuildings() {
        mapViewModel.getBuildings()
    }

    override fun onStart() {
        super.onStart()
        if (ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            checkSettingsAndStartLocationUpdates()
        } else {
            askLocationPermission()
        }
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
    }

    private fun checkSettingsAndStartLocationUpdates() {
        val request = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest).build()
        val client = LocationServices.getSettingsClient(activity!!)
        val locationSettingsResponseTask = client.checkLocationSettings(request)
        locationSettingsResponseTask.addOnSuccessListener { //Settings of device are satisfied and we can start location updates
            startLocationUpdates()
        }
        locationSettingsResponseTask.addOnFailureListener { e ->
            if (e is ResolvableApiException) {
                try {
                    e.startResolutionForResult(activity!!, 1)
                } catch (ex: SendIntentException) {
                    ex.printStackTrace()
                }
            }
        }
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
    }

    private fun stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
    }

    private fun askLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                Log.d(TAG, "askLocationPermission: you should show an alert dialog...")
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            1 -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkSettingsAndStartLocationUpdates()
            }
        }
    }

    override fun onMapClick(latLng: LatLng) {
        binding.backgroundBuilding.isVisible = false
        binding.buildingName.isVisible = false
        binding.buildingInfo.isVisible = false
        binding.buildingImage.isVisible = false
        binding.greyBackground.isVisible = false
        binding.botonAccedeEdificio.isVisible = false
    }

    fun invisbleAllComponents() {
        binding.backgroundBuilding.isVisible = false
        binding.buildingName.isVisible = false
        binding.buildingInfo.isVisible = false
        binding.buildingImage.isVisible = false
        binding.greyBackground.isVisible = false
        binding.botonAccedeEdificio.isVisible = false
    }

    @SuppressLint("InflateParams")
    fun onButtonShowPopupWindowClick(points: Int) {
        val popupView = layoutInflater.inflate(R.layout.popup_window, null)
        val popupText = popupView.findViewById<TextView>(R.id.popupText)
        popupText.text = "Has ganado " + points + " puntos"

        // create the popup window
        val width: Int = LinearLayout.LayoutParams.WRAP_CONTENT
        val height: Int = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = true // lets taps outside the popup also dismiss it
        val popupWindow = PopupWindow(popupView, width, height, focusable)

        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
    }

}