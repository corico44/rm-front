package com.reus_modernista_front.view.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.reus_modernista_front.R
import com.reus_modernista_front.databinding.FragmentProfileBinding
import com.reus_modernista_front.db.Session
import com.reus_modernista_front.db.UserDB
import com.reus_modernista_front.view.login.LoginActivity


class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val userDAO = UserDB.getDataBase(activity?.application!!).userDAO()
        profileViewModel = ProfileViewModel(userDAO)

        binding = FragmentProfileBinding.inflate(inflater, container, false)
        //set variables in Binding
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileViewModel.readAllData.observe(this, Observer { currentUser ->
            if (currentUser != null) {
                binding.nombreCuentaValor.text = currentUser.name
                binding.emailCuentaValor.text = currentUser.email
                binding.puntosCuentaValor.text = currentUser.points.toString()
            }

        })

        binding.logoutButton.setOnClickListener() {
            if (Session.getIsGoogleAccount()) {
                logout()
            }
            val intent = Intent(activity, LoginActivity::class.java)
            startActivity(intent)
            activity!!.finish()
        }
    }

    private fun logout() {
        val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(activity!!.getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        val googleSignInClient = GoogleSignIn.getClient(activity!!, googleSignInOptions)
        googleSignInClient.signOut()
    }

}