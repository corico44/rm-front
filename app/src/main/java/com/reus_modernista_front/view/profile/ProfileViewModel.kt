package com.reus_modernista_front.view.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.reus_modernista_front.db.UserDAO
import com.reus_modernista_front.db.entity.UserEntity
import com.reus_modernista_front.repository.UserRepository

class ProfileViewModel(userDAO: UserDAO) : ViewModel() {

    val readAllData: LiveData<UserEntity>

    private val userRepository = UserRepository(userDAO)

    init {
        readAllData = userRepository.readAllData
    }
}