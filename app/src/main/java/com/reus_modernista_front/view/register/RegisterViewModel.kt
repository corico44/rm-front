package com.reus_modernista_front.view.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.reus_modernista_front.db.Session
import com.reus_modernista_front.db.UserDAO
import com.reus_modernista_front.db.entity.UserEntity
import com.reus_modernista_front.model.user.UserResponse
import com.reus_modernista_front.model.user.authUser.register.RegisterAuthUser
import com.reus_modernista_front.model.user.authUser.register.RegisterUserRequest
import com.reus_modernista_front.repository.UserRepository
import com.reus_modernista_front.util.SecurityMethods
import kotlinx.coroutines.launch
import retrofit2.Response

class RegisterViewModel(userDAO: UserDAO) : ViewModel() {
    val user = MutableLiveData<RegisterAuthUser>()
    val response = MutableLiveData<UserResponse>()

    private val userRepository = UserRepository(userDAO)

    fun register(registerUser: RegisterAuthUser, isGoogleRegister: Boolean) {
        viewModelScope.launch {
            if (userRepository.userExists(registerUser.email)) {
                response.value = UserResponse("409", "Conflict")
            } else {
                val registerUserRequest = SecurityMethods.encrypt(registerUser.password)
                    ?.let { RegisterUserRequest(registerUser.name, registerUser.email, it) }
                val res: Response<UserResponse> = userRepository.register(registerUserRequest!!)

                if (res.isSuccessful) {

                    val userRes: UserResponse? = res.body()

                    if ((userRes != null) && (userRes.code == "201")) {
                        Session.loginUserLocalDB(registerUserRequest.email, isGoogleRegister)

                        val userEntity = UserEntity(
                            registerUserRequest.email, registerUserRequest.name,
                            registerUserRequest.password, 0, null
                        )
                        userRepository.addUser(userEntity)
                    }
                }
                response.value = res.body()?.let { UserResponse(it.code, res.body()!!.message) }
            }
        }
    }

    fun isNameValid(registerUser: RegisterAuthUser) {
        val nameIsValid = registerUser.isNameValid(registerUser.name)
        user.value = RegisterAuthUser(
            registerUser.name,
            registerUser.email,
            registerUser.password,
            nameIsValid,
            registerUser.isEmailValid,
            registerUser.isPasswordValid
        )
    }

    fun isEmailValid(registerUser: RegisterAuthUser) {
        val emailIsValid = registerUser.isEmailValid(registerUser.email)
        user.value = RegisterAuthUser(
            registerUser.name,
            registerUser.email,
            registerUser.password,
            registerUser.isNameValid,
            emailIsValid,
            registerUser.isPasswordValid
        )
    }

    fun isPasswordValid(registerUser: RegisterAuthUser) {
        val passwordIsValid = registerUser.isPasswordValid(registerUser.password)
        user.value = RegisterAuthUser(
            registerUser.name,
            registerUser.email,
            registerUser.password,
            registerUser.isNameValid,
            registerUser.isEmailValid,
            passwordIsValid
        )
    }
}