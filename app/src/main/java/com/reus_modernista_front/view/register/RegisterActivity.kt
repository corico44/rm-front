package com.reus_modernista_front.view.auth.login.register

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.reus_modernista_front.NavActivity
import com.reus_modernista_front.R
import com.reus_modernista_front.databinding.ActivityRegisterBinding
import com.reus_modernista_front.db.UserDB
import com.reus_modernista_front.model.user.authUser.register.RegisterAuthUser
import com.reus_modernista_front.util.ConnectionCheck.isConnectedToInternet
import com.reus_modernista_front.view.login.LoginActivity
import com.reus_modernista_front.view.register.RegisterViewModel

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var registerViewModel: RegisterViewModel
    private var isNameValid: Boolean = false
    private var isEmailValid: Boolean = false
    private var isPasswordValid: Boolean = false

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var googleSignInOptions: GoogleSignInOptions

    private companion object {
        private const val RC_SIGN_IN = 100
        private const val TAG = "GOOGLE_SIGN_IN_TAG"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
        firebaseAuth = FirebaseAuth.getInstance()

        binding.signin.setOnClickListener() {
            Log.d(TAG, "Begin google signing")
            val intent = googleSignInClient.signInIntent
            startActivityForResult(intent, RC_SIGN_IN)

        }

        val userDAO = UserDB.getDataBase(application).userDAO()
        registerViewModel = RegisterViewModel(userDAO)

        binding.CreateAccButto.text =
            Html.fromHtml(
                "<font color='#FF5757'>¿Ya tienes cuenta? </font>"
                        + "<font color='#828282'> Accede ahora</font>"
            )

        registerViewModel.user.observe(this) { currentUser ->
            if (!currentUser.isNameValid) {
                checkIfNameIsValid(currentUser)
            }
            if (!currentUser.isEmailValid) {
                checkIfEmailIsValid(currentUser)
            }
            if (!currentUser.isPasswordValid) {
                checkIfPasswordIsValid(currentUser)
            }
            if (currentUser.isNameValid) {
                binding.nombreRegistro.error = null
                isNameValid = true
            }
            if (currentUser.isEmailValid) {
                binding.emailRegistro.error = null
                isEmailValid = true
            }
            if (currentUser.isPasswordValid) {
                binding.contrasenaRegistro.error = null
                isPasswordValid = true
            }
        }

        registerViewModel.response.observe(this, Observer {
            if (it != null && "201" == it.code) {
                val intent = Intent(this, NavActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Toast.makeText(applicationContext, "¡El usuario ya existe, prueba con otro email!", Toast.LENGTH_SHORT)
                    .show()
            }
        })

        binding.creaMiCuenta.setOnClickListener() {
            val isConnected = isConnectedToInternet(applicationContext)
            if(isConnected){
                val registerUser = RegisterAuthUser(
                    binding.nombreRegistroCampo.text.toString(),
                    binding.emailRegistroCampo.text.toString(),
                    binding.contrasenaRegistroCampo.text.toString(),
                    isNameValid,
                    isEmailValid,
                    isPasswordValid
                )
                if (isNameValid && isEmailValid && isPasswordValid) {
                    registerViewModel.register(registerUser, false)
                } else {
                    Toast.makeText(applicationContext, "¡Los datos no tienen el formato correcto!", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            else{
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }

        binding.CreateAccButto.setOnClickListener() {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        binding.nombreRegistroCampo.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val registerUser = RegisterAuthUser(
                    binding.nombreRegistroCampo.text.toString(),
                    binding.emailRegistroCampo.text.toString(),
                    binding.contrasenaRegistroCampo.text.toString(),
                    isNameValid,
                    isEmailValid,
                    isPasswordValid
                )
                registerViewModel.isNameValid(registerUser)
            }
        }

        binding.emailRegistroCampo.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val registerUser = RegisterAuthUser(
                    binding.nombreRegistroCampo.text.toString(),
                    binding.emailRegistroCampo.text.toString(),
                    binding.contrasenaRegistroCampo.text.toString(),
                    isNameValid,
                    isEmailValid,
                    isPasswordValid
                )
                registerViewModel.isEmailValid(registerUser)
            }
        }

        binding.contrasenaRegistroCampo.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val registerUser = RegisterAuthUser(
                    binding.nombreRegistroCampo.text.toString(),
                    binding.emailRegistroCampo.text.toString(),
                    binding.contrasenaRegistroCampo.text.toString(),
                    isNameValid,
                    isEmailValid,
                    isPasswordValid
                )
                registerViewModel.isPasswordValid(registerUser)
            }
        }
    }

    private fun checkIfNameIsValid(currentUser: RegisterAuthUser) {
        binding.nombreRegistro.requestFocus()
        if (currentUser.name.isEmpty()) {
            binding.nombreRegistroCampo.error = "El nombre está está vacío"
        }
    }

    private fun checkIfPasswordIsValid(currentUser: RegisterAuthUser) {
        binding.contrasenaRegistro.requestFocus()
        if (currentUser.password.isEmpty()) {
            binding.contrasenaRegistroCampo.error = "La contraseña está vacía"
        } else {
            binding.contrasenaRegistroCampo.error = "La contraseña tiene menos de 5 caracteres"
        }
    }

    private fun checkIfEmailIsValid(currentUser: RegisterAuthUser) {
        binding.emailRegistro.requestFocus()
        if (currentUser.email.isEmpty()) {
            binding.emailRegistroCampo.error = "El email está vacío"
        } else if ("@" !in currentUser.email) {
            binding.emailRegistroCampo.error = "El email no tiene el simbolo del arroba"
        } else if (".com" !in currentUser.email) {
            binding.emailRegistroCampo.error = "El email no tiene el .com o .es"
        } else {
            binding.emailRegistroCampo.error = "El email no tiene el formato correcto"
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            Log.d(TAG, "Google singing intent result")
            val accountTask = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = accountTask.getResult(ApiException::class.java)
                firebaseAuthWithGoogleAccount(account)
            } catch (e: Exception) {
                Log.d(TAG, "onActivityResult: ${e.message}")
            }
        }
    }

    private fun firebaseAuthWithGoogleAccount(account: GoogleSignInAccount?) {
        Log.d(TAG, "Begin firebase auth with google account")

        val credential = GoogleAuthProvider.getCredential(account!!.idToken, null)
        firebaseAuth.signInWithCredential(credential)
            .addOnSuccessListener { authResult ->
                Log.d(TAG, "Loggedin!")
                val firebaseUser = firebaseAuth.currentUser
                val email = firebaseUser!!.email

                val acct = GoogleSignIn.getLastSignedInAccount(this)

                Log.d(TAG, "firebaseAuthWithGoogleAccount: email: $email")

                if (authResult.additionalUserInfo!!.isNewUser) {
                    Log.d(TAG, "Account created...\n$email")
                    val registerUser = acct!!.givenName?.let {
                        RegisterAuthUser(
                            it,
                            email!!,
                            "",
                            true,
                            true,
                            true
                        )
                    }
                    registerViewModel.register(registerUser!!, true)
                } else {
                    Log.d(TAG, "Existing user $email")
                    Toast.makeText(applicationContext, "¡El usuario ya existe!", Toast.LENGTH_SHORT).show()

                    val googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
                    googleSignInClient.signOut()
                }
            }
            .addOnFailureListener() { e ->
                Log.d(TAG, "Signin failed due to ${e.message}")
                Toast.makeText(applicationContext, "Error al registrarse", Toast.LENGTH_SHORT).show()
            }
    }
}