package com.reus_modernista_front.view.buildingQuestions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.reus_modernista_front.db.UserDAO
import com.reus_modernista_front.model.user.addPoints.PointsUser
import com.reus_modernista_front.model.user.addVisitedBuilding.VisitedBuildingUser
import com.reus_modernista_front.repository.UserRepository
import kotlinx.coroutines.launch


class BuildingQuestionsViewModel(userDAO: UserDAO) : ViewModel() {

    private val userRepository = UserRepository(userDAO)

    fun addPoints(pointsUser: PointsUser) {
        viewModelScope.launch {
            userRepository.addLocalPoints(pointsUser.points)
            userRepository.addPoints(pointsUser)
        }
    }

    fun addVisitedBuilding(visitedBuildingUser: VisitedBuildingUser) {
        viewModelScope.launch {
            userRepository.addVisitedBuilding(visitedBuildingUser)
            var visitedBuildings = userRepository.getLocalVisitedBuilding(visitedBuildingUser.email)
            if (visitedBuildings.isEmpty()) visitedBuildings += visitedBuildingUser.visitedBuildingName
            else visitedBuildings += "," + visitedBuildingUser.visitedBuildingName
            userRepository.addLocalVisitedBuilding(visitedBuildingUser.email, visitedBuildings)
        }
    }
}