package com.reus_modernista_front.view.buildingQuestions

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.reus_modernista_front.NavActivity
import com.reus_modernista_front.R
import com.reus_modernista_front.databinding.ActivityBuildingQuestionsBinding
import com.reus_modernista_front.db.Session
import com.reus_modernista_front.db.UserDB
import com.reus_modernista_front.model.user.addPoints.PointsUser
import com.reus_modernista_front.model.user.addVisitedBuilding.VisitedBuildingUser


class BuildingQuestionsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBuildingQuestionsBinding
    private lateinit var buildingQuestionsViewModel: BuildingQuestionsViewModel
    private lateinit var buildingName: String
    private var correctAnswer: Int = 0
    private var count: Int = 0
    private var points: Int = 0
    private var totalTime: Long = 60000
    private var totalQuestions: Int = 0
    private lateinit var timer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBuildingQuestionsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val userDAO = UserDB.getDataBase(application!!).userDAO()
        buildingQuestionsViewModel = BuildingQuestionsViewModel(userDAO)

        val extras = intent.extras
        if (extras != null) {
            buildingName = extras.getString("buildingName").toString()
        }

        ++count

//        val numberQuestionsName = (buildingName).replace(" ", "_").lowercase() + "_number_questions"
//        val  numberQuestionsResource = resources.getIdentifier(numberQuestionsName, "string", packageName)
//        totalQuestions = (resources.getText(numberQuestionsResource) as String).toInt()

        nextQuestion()

        binding.questionsBackground.setBackgroundColor(resources.getColor(R.color.light_blue))

        binding.firstAnswer.setOnClickListener {
            if (correctAnswer == 1) {
                addPoints()
                ++count
                nextQuestion()
            } else {
                Toast.makeText(applicationContext, "¡No es correcto!", Toast.LENGTH_SHORT).show()
                timer.cancel()
                if (totalTime >= 10000) totalTime -= 10000
                setTimer()
            }
        }
        binding.secondAnswer.setOnClickListener {
            if (correctAnswer == 2) {
                addPoints()
                ++count
                nextQuestion()
            } else {
                Toast.makeText(applicationContext, "¡No es correcto!", Toast.LENGTH_SHORT).show()
                timer.cancel()
                if (totalTime >= 10000) totalTime -= 10000
                setTimer()
            }
        }
        binding.thirdAnswer.setOnClickListener {
            if (correctAnswer == 3) {
                addPoints()
                ++count
                nextQuestion()
            } else {
                Toast.makeText(applicationContext, "¡No es correcto!", Toast.LENGTH_SHORT).show()
                timer.cancel()
                if (totalTime >= 10000) totalTime -= 10000
                setTimer()
            }
        }
    }

    private fun setTimer() {

        timer = object : CountDownTimer(totalTime, 1000) {

            // Callback function, fired on regular interval
            override fun onTick(millisUntilFinished: Long) {
                totalTime -= 1000
                binding.timer.text = (millisUntilFinished / 1000).toString()
            }

            // Callback function, fired
            // when the time is up
            override fun onFinish() {
                timer.cancel()
                ++count
                nextQuestion()
            }
        }.start()
    }

    private fun nextQuestion() {
        try {
            val questionTextName = "question_" + count.toString() + "_" + (buildingName).replace(" ", "_").lowercase()
            val firstAnswerTextName =
                "answer_" + count.toString() + "_" + (buildingName).replace(" ", "_").lowercase() + "_1"
            val secondAnswerTextName =
                "answer_" + count.toString() + "_" + (buildingName).replace(" ", "_").lowercase() + "_2"
            val thirdAnswerTextName =
                "answer_" + count.toString() + "_" + (buildingName).replace(" ", "_").lowercase() + "_3"

            val questionTextResource = resources.getIdentifier(questionTextName, "string", packageName)
            val firstAnswerResource = resources.getIdentifier(firstAnswerTextName, "string", packageName)
            val secondAnswerResource = resources.getIdentifier(secondAnswerTextName, "string", packageName)
            val thirdAnswerResource = resources.getIdentifier(thirdAnswerTextName, "string", packageName)

            binding.question.text = resources.getText(questionTextResource) as String

            val firstAnswerText = (resources.getText(firstAnswerResource) as String).split("-")
            val secondAnswerText = (resources.getText(secondAnswerResource) as String).split("-")
            val thirdAnswerText = (resources.getText(thirdAnswerResource) as String).split("-")

            if ("true" == firstAnswerText[1]) correctAnswer = 1
            else if ("true" == secondAnswerText[1]) correctAnswer = 2
            else if ("true" == thirdAnswerText[1]) correctAnswer = 3

            binding.firstAnswer.text = firstAnswerText[0]
            binding.secondAnswer.text = secondAnswerText[0]
            binding.thirdAnswer.text = thirdAnswerText[0]
            totalTime = 60000
            if (count.toString() != "1") timer.cancel()
            setTimer()

        } catch (e: Resources.NotFoundException) {
            timer.cancel()
            val pointsUser = PointsUser(Session.getEmailUsuario(), points)
            buildingQuestionsViewModel.addPoints(pointsUser)

            val visitedBuildingUser = VisitedBuildingUser(Session.getEmailUsuario(), buildingName)
            buildingQuestionsViewModel.addVisitedBuilding(visitedBuildingUser)

            val intent = Intent(this, NavActivity::class.java)
            intent.putExtra("points", points.toString())
            startActivity(intent)
            finish()
        }
    }

    private fun addPoints() {
        points += (binding.timer.text.toString().toInt() * 100) / 60
    }

}
