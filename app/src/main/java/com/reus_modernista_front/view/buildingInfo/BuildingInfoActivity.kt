package com.reus_modernista_front.view.buildingInfo

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.reus_modernista_front.R
import com.reus_modernista_front.databinding.ActivityBuildingInfoBinding
import com.reus_modernista_front.view.buildingQuestions.BuildingQuestionsActivity
import kotlin.properties.Delegates


class BuildingInfoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBuildingInfoBinding
    private lateinit var buildingName: String
    private lateinit var buildingArchitect: String
    private var count by Delegates.notNull<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBuildingInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.playButton.isVisible = false

        val extras = intent.extras
        if (extras != null) {
            buildingName = extras.getString("buildingName").toString()
            buildingArchitect = extras.getString("buildingArchitect").toString()
        }

        binding.buildingName.text = buildingName
        binding.buildingArchitectName.text = buildingArchitect

        count = 1

        val firstBuildingInfoText = "text_" + (buildingName).replace(" ", "_").lowercase() + "_" + count.toString()
        val firstBuildingInfoTextResource = resources.getIdentifier(firstBuildingInfoText, "string", packageName)
        binding.buildingInfoFirst.text = resources.getText(firstBuildingInfoTextResource) as String

        binding.nextInfoButton.setOnClickListener() {
            val viewInfoText = layoutInflater.inflate(R.layout.layout_text_info, binding.infoArea, false)

            ++count

            try {
                val buildingInfoText = "text_" + (buildingName).replace(" ", "_").lowercase() + "_" + count.toString()
                val buildingInfoTextResource = resources.getIdentifier(buildingInfoText, "string", packageName)

                val buildingInfo: TextView = viewInfoText.findViewById<View>(R.id.buildingInfoSecond) as TextView
                buildingInfo.text =
                    resources.getText(buildingInfoTextResource) as String

                val architectName: TextView = viewInfoText.findViewById<View>(R.id.buildingArchitectName) as TextView
                architectName.text = buildingArchitect
                binding.infoArea.addView(viewInfoText)

            } catch (e: Resources.NotFoundException) {
                binding.nextInfoButton.isVisible = false
                binding.playButton.isVisible = true
            }
        }

        binding.playButton.setOnClickListener {
            val intent = Intent(this, BuildingQuestionsActivity::class.java)
            intent.putExtra("buildingName", binding.buildingName.text)
            startActivity(intent)
            finish()
        }
    }
}
