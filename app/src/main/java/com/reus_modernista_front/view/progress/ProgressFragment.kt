package com.reus_modernista_front.view.progress

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.reus_modernista_front.databinding.FragmentProgressBinding
import com.reus_modernista_front.db.Session
import com.reus_modernista_front.db.UserDB
import com.reus_modernista_front.util.ConnectionCheck
import kotlin.math.roundToInt


class ProgressFragment : Fragment() {

    private lateinit var binding: FragmentProgressBinding
    private lateinit var progressViewModel: ProgressViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProgressBinding.inflate(inflater, container, false)
        //set variables in Binding

        val userDAO = UserDB.getDataBase(this.activity!!).userDAO()
        progressViewModel = ProgressViewModel(userDAO)

        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val isConnected = ConnectionCheck.isConnectedToInternet(activity!!.applicationContext)
        if(isConnected){
            progressViewModel.getSizeVisitedBuilding(Session.getEmailUsuario())
        }
        else if(!isConnected) Toast.makeText(activity,"No hay conexión a internet. ¡Actívala para poder ver tu progreso!",
            Toast.LENGTH_SHORT).show()

        progressViewModel.response.observe(this, Observer {
            var progress: Int = (it * 100) / 26
            progress = ((progress / 10.0).roundToInt() * 10)

            if (progress == 100 && it != 26) {
                progress = 90
            }

            if (progress == 100) binding.progressMessage.text = "¡Has completado toda la ruta!"
            else binding.progressMessage.text = "¡Has completado un $progress% de la ruta!"

            binding.progressImage.setImageResource(
                resources.getIdentifier(
                    "progress_$progress",
                    "drawable",
                    activity!!.packageName
                )
            )

        })
    }
}