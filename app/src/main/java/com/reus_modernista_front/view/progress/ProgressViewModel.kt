package com.reus_modernista_front.view.progress

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.reus_modernista_front.db.UserDAO
import com.reus_modernista_front.repository.UserRepository
import kotlinx.coroutines.launch


class ProgressViewModel(userDAO: UserDAO) : ViewModel() {

    private val userRepository = UserRepository(userDAO)
    var response = MutableLiveData<Int>()

    fun getSizeVisitedBuilding(email: String) {
        viewModelScope.launch {
            val visitedBuildings = userRepository.getLocalVisitedBuilding(email).split(',')
            response.value = visitedBuildings.size
        }
    }
}