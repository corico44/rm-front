package com.reus_modernista_front.view.ranking

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.Html
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.reus_modernista_front.R
import com.reus_modernista_front.databinding.FragmentRankingBinding
import com.reus_modernista_front.db.UserDB
import com.reus_modernista_front.db.entity.UserEntity
import com.reus_modernista_front.model.user.getRanking.GetRankingUserResponse
import com.reus_modernista_front.util.ConnectionCheck.isConnectedToInternet


class RankingFragment : Fragment() {

    private lateinit var binding: FragmentRankingBinding
    private lateinit var rankingViewModel: RankingViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val userDAO = UserDB.getDataBase(activity?.application!!).userDAO()
        rankingViewModel = RankingViewModel(userDAO)

        binding = FragmentRankingBinding.inflate(inflater, container, false)
        disableComponents()
        val isConnected = isConnectedToInternet(activity!!.applicationContext)
        if(isConnected){
            rankingViewModel.getAllUsers()
        }
        else if(!isConnected) Toast.makeText(activity,"No hay conexión a internet. ¡Actívala para poder ver el ranking!",Toast.LENGTH_SHORT).show()

        rankingViewModel.response.observe(this, Observer {
            init(it)
        })
        //set variables in Binding
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    fun init(users: List<GetRankingUserResponse>) {
        var count = 1
        for (user in users) {
            val sourceString = "<big><b>" + user.name + "</b></big>" + "\n ${user.points}"
            if (count == 1) {
                binding.firstRankingView.isVisible = true
                binding.firstRankingNumber.isVisible = true
                binding.firstRankingValue.isVisible = true
                binding.firstRankingValue.text = Html.fromHtml(sourceString)
            } else if (count == 2) {
                binding.secondRankingView.isVisible = true
                binding.secondRankingNumber.isVisible = true
                binding.secondRankingValue.isVisible = true
                binding.secondRankingValue.text = Html.fromHtml(sourceString)
            } else if (count == 3) {
                binding.thirdRankingView.isVisible = true
                binding.thirdRankingNumber.isVisible = true
                binding.thirdRankingValue.isVisible = true
                binding.thirdRankingValue.text = Html.fromHtml(sourceString)
            } else {
                val tbrow = TableRow(activity)
                val t1v = TextView(activity)
                t1v.text = user.name
                t1v.setTextColor(Color.BLACK)
                t1v.gravity = Gravity.CENTER
                t1v.textSize = 17F
                t1v.setTypeface(null, Typeface.BOLD)
                t1v.setPadding(200, 50, 300, 50)
                tbrow.addView(t1v)
                val t2v = TextView(activity)
                t2v.text = user.points.toString()
                t2v.setTextColor(Color.BLACK)
                t2v.gravity = Gravity.CENTER
                t2v.textSize = 17F
                t2v.setPadding(0, 50, 200, 50)
                tbrow.addView(t2v)
                tbrow.background = context?.let { ContextCompat.getDrawable(it, R.drawable.white_button) }

                val tableRowParams: TableLayout.LayoutParams =
                    TableLayout.LayoutParams(
                        TableLayout.LayoutParams.FILL_PARENT,
                        TableLayout.LayoutParams.WRAP_CONTENT
                    )

                val leftMargin = 0
                val topMargin = 0
                val rightMargin = 0
                val bottomMargin = 50

                tableRowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin)

                tbrow.layoutParams = tableRowParams

                binding.tableMain.addView(tbrow)
            }
            ++count
        }
        binding.tableMain.setPadding(0, 0, 0, 400)
    }

    fun disableComponents() {
        binding.firstRankingView.isVisible = false
        binding.firstRankingNumber.isVisible = false
        binding.firstRankingValue.isVisible = false

        binding.secondRankingView.isVisible = false
        binding.secondRankingNumber.isVisible = false
        binding.secondRankingValue.isVisible = false

        binding.thirdRankingView.isVisible = false
        binding.thirdRankingNumber.isVisible = false
        binding.thirdRankingValue.isVisible = false
    }

}