package com.reus_modernista_front.view.ranking

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.reus_modernista_front.db.UserDAO
import com.reus_modernista_front.model.user.getRanking.GetRankingUserResponse
import com.reus_modernista_front.repository.UserRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class RankingViewModel(userDAO: UserDAO) : ViewModel() {


    private val userRepository = UserRepository(userDAO)
    val response = MutableLiveData<List<GetRankingUserResponse>>()

    fun getAllUsers() {
        viewModelScope.launch {
            val res: Response<List<GetRankingUserResponse>> = userRepository.getAllUsers()

            if (res.isSuccessful) {

                val userRes: List<GetRankingUserResponse>? = res.body()

                if (userRes != null) {
                    response.value = res.body()
                }
            }
        }
    }
}