package com.reus_modernista_front.view.login

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.View.OnFocusChangeListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.reus_modernista_front.NavActivity
import com.reus_modernista_front.R
import com.reus_modernista_front.databinding.ActivityLoginBinding
import com.reus_modernista_front.db.UserDB
import com.reus_modernista_front.model.user.authUser.login.LoginAuthUser
import com.reus_modernista_front.util.ConnectionCheck
import com.reus_modernista_front.view.auth.login.register.RegisterActivity


class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var loginViewModel: LoginViewModel
    private var isEmailValid: Boolean = false
    private var isPasswordValid: Boolean = false

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var googleSignInOptions: GoogleSignInOptions

    private companion object {
        private const val RC_SIGN_IN = 100
        private const val TAG = "GOOGLE_SIGN_IN_TAG"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
        firebaseAuth = FirebaseAuth.getInstance()

        binding.login.setOnClickListener() {
            Log.d(TAG, "Begin google signin")
            val intent = googleSignInClient.signInIntent
            startActivityForResult(intent, RC_SIGN_IN)

        }

        val userDAO = UserDB.getDataBase(application).userDAO()
        loginViewModel = LoginViewModel(userDAO)

        binding.CreateAccButto.text =
            Html.fromHtml(
                "<font color='#FF5757'>¿Aún no tienes cuenta? </font>"
                        + "<font color='#828282'> Créala ahora</font>"
            )

        loginViewModel.user.observe(this, Observer { currentUser ->
            if (!currentUser.isEmailValid) {
                checkIfEmailIsValid(currentUser)
            }
            if (!currentUser.isPasswordValid) {
                checkIfPasswordIsValid(currentUser)
            }
            if (currentUser.isEmailValid) {
                binding.emailRegistro.error = null
                isEmailValid = true
            }
            if (currentUser.isPasswordValid) {
                binding.contrasenaRegistro.error = null
                isPasswordValid = true
            }
        })

        loginViewModel.response.observe(this, Observer {
            if (it != null && "200" == it.code) {
                val intent = Intent(this, NavActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                Toast.makeText(applicationContext, "¡El email o la contraseña no son correctos!", Toast.LENGTH_SHORT)
                    .show()
            }
        })

        binding.CreateAccButto.setOnClickListener() {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            finish()
        }

        binding.button.setOnClickListener() {
            val isConnected = ConnectionCheck.isConnectedToInternet(applicationContext)
            if(isConnected){
                val loginUser = LoginAuthUser(
                    binding.emailRegistroCampo.text.toString(),
                    binding.contrasenaRegistroCampo.text.toString(),
                    isEmailValid,
                    isPasswordValid
                )
                loginViewModel.login(loginUser, false)
            }
        }

        binding.emailRegistroCampo.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val loginUser = LoginAuthUser(
                    binding.emailRegistroCampo.text.toString(),
                    binding.contrasenaRegistroCampo.text.toString(),
                    isEmailValid,
                    isPasswordValid
                )
                loginViewModel.isEmailValid(loginUser)
            }
        }

        binding.contrasenaRegistroCampo.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            if (!hasFocus) {
                val loginUser = LoginAuthUser(
                    binding.emailRegistroCampo.text.toString(),
                    binding.contrasenaRegistroCampo.text.toString(),
                    isEmailValid,
                    isPasswordValid
                )
                loginViewModel.isPasswordValid(loginUser)
            }
        }
    }

    private fun checkIfPasswordIsValid(currentUser: LoginAuthUser) {
        binding.contrasenaRegistro.requestFocus()
        if (currentUser.password.isEmpty()) {
            binding.contrasenaRegistroCampo.error = "La contraseña está vacía"
        } else {
            binding.contrasenaRegistroCampo.error = "La contraseña tiene menos de 5 caracteres"
        }
    }

    private fun checkIfEmailIsValid(currentUser: LoginAuthUser) {
        binding.emailRegistro.requestFocus()
        if (currentUser.email.isEmpty()) {
            binding.emailRegistroCampo.error = "El email está vacío"
        } else if ("@" !in currentUser.email) {
            binding.emailRegistroCampo.error = "El email no tiene el simbolo del arroba"
        } else if (".com" !in currentUser.email) {
            binding.emailRegistroCampo.error = "El email no tiene el .com o .es"
        } else {
            binding.emailRegistroCampo.error = "El email no tiene el formato correcto"
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            Log.d(TAG, "onActivityResult: google singing intent result")
            val accountTask = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = accountTask.getResult(ApiException::class.java)
                firebaseAuthWithGoogleAccount(account)
            } catch (e: Exception) {
                Log.d(TAG, "onActivityResult: ${e.message}")
            }
        }
    }

    private fun firebaseAuthWithGoogleAccount(account: GoogleSignInAccount?) {
        Log.d(TAG, "Begin firebase auth with google account")

        val credential = GoogleAuthProvider.getCredential(account!!.idToken, null)
        firebaseAuth.signInWithCredential(credential)
            .addOnSuccessListener { authResult ->
                val firebaseUser = firebaseAuth.currentUser
                val email = firebaseUser!!.email

                if (!authResult.additionalUserInfo!!.isNewUser) {
                    Log.d(TAG, "Login success with email: $email")
                    val loginUser = LoginAuthUser(email!!, "", true, true)
                    loginViewModel.login(loginUser, true)
                } else {
                    deleteAccount()
                    val googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
                    googleSignInClient.signOut()
                    Toast.makeText(applicationContext, "¡El usuario no existe!", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener() { e ->
                Log.d(TAG, "Failed login: ${e.message}")
                Toast.makeText(applicationContext, "¡Error al registrarse con Google!", Toast.LENGTH_SHORT).show()
            }
    }

    private fun deleteAccount() {
        val firebaseAuth = FirebaseAuth.getInstance()
        val currentUser = firebaseAuth.currentUser
        currentUser!!.delete().addOnCompleteListener { task ->
            if (task.isSuccessful) {
            } else {
                Log.w(TAG, "Something went wrong while deleting user on Firebase")
            }
        }
    }
}
