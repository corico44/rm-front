package com.reus_modernista_front.view.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.reus_modernista_front.db.Session
import com.reus_modernista_front.db.UserDAO
import com.reus_modernista_front.db.entity.UserEntity
import com.reus_modernista_front.model.user.UserResponse
import com.reus_modernista_front.model.user.authUser.login.LoginAuthUser
import com.reus_modernista_front.model.user.authUser.login.LoginUserRequest
import com.reus_modernista_front.model.user.getInfo.GetInfoUserResponse
import com.reus_modernista_front.repository.UserRepository
import com.reus_modernista_front.util.SecurityMethods
import kotlinx.coroutines.launch
import retrofit2.Response


class LoginViewModel(userDAO: UserDAO) : ViewModel() {

    val user = MutableLiveData<LoginAuthUser>()
    val response = MutableLiveData<UserResponse>()

    private val userRepository = UserRepository(userDAO)

    fun login(loginUser: LoginAuthUser, isGoogleLogin: Boolean) {
        viewModelScope.launch {
            if (SecurityMethods.encrypt(loginUser.password)
                    ?.let { userRepository.userIsLogged(loginUser.email, it) } == true
            ) {
                getInfoUser(loginUser.email)

                Session.loginUserLocalDB(loginUser.email, isGoogleLogin)
                response.value = UserResponse("200", "UserEntity exists")
            } else {
                val res: Response<UserResponse>? = SecurityMethods.encrypt(loginUser.password)
                    ?.let { LoginUserRequest(loginUser.email, it) }?.let { userRepository.login(it) }

                if (res!!.isSuccessful) {

                    val userRes: UserResponse? = res.body()

                    if ((userRes != null) && (userRes.code == "200")) {
                        getInfoUser(loginUser.email)

                        Session.loginUserLocalDB(loginUser.email, isGoogleLogin)
                    }
                }
                response.value = res.body()?.let { UserResponse(it.code, res.body()!!.message) }
            }
        }
    }

    fun isEmailValid(loginUser: LoginAuthUser) {
        val emailIsValid = loginUser.isEmailValid(loginUser.email)
        user.value = LoginAuthUser(loginUser.email, loginUser.password, emailIsValid, loginUser.isPasswordValid)
    }

    fun isPasswordValid(loginUser: LoginAuthUser) {
        val passwordIsValid = loginUser.isPasswordValid(loginUser.password)
        user.value = LoginAuthUser(loginUser.email, loginUser.password, loginUser.isEmailValid, passwordIsValid)
    }

    private fun getInfoUser(email: String) {
        viewModelScope.launch {
            val res: Response<GetInfoUserResponse> = userRepository.getInfoUser(email)
            if (res.isSuccessful) {
                val userRes: GetInfoUserResponse? = res.body()

                if (userRes != null) {
                    val userEntity = UserEntity(
                        userRes.email, userRes.name,
                        userRes.password, userRes.points, userRes.visited
                    )
                    userRepository.addUser(userEntity)
                }
            }
        }
    }
}