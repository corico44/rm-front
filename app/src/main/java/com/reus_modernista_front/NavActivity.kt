package com.reus_modernista_front

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.reus_modernista_front.databinding.ActivityNavBinding
import com.reus_modernista_front.view.map.MapFragment
import com.reus_modernista_front.view.profile.ProfileFragment
import com.reus_modernista_front.view.progress.ProgressFragment
import com.reus_modernista_front.view.ranking.RankingFragment

class NavActivity : AppCompatActivity() {

    private val rankingFragment = RankingFragment()
    private val mapFragment = MapFragment()
    private val progressFragment = ProgressFragment()
    private val profileFragment = ProfileFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nav)

        val navView: BottomNavigationView = findViewById(R.id.bottom_navigation)

        replaceFragment(mapFragment)

        navView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.ic_map -> replaceFragment(mapFragment)
                R.id.ic_ranking -> replaceFragment(rankingFragment)
                R.id.ic_badge -> replaceFragment(progressFragment)
                R.id.ic_profile -> replaceFragment(profileFragment)
            }
            true
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        if (fragment != null) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, fragment)
            transaction.commit()
        }
    }
}