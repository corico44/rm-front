package com.reus_modernista_front.repository

import androidx.lifecycle.LiveData
import com.reus_modernista_front.api.RetrofitInstance
import com.reus_modernista_front.db.Session
import com.reus_modernista_front.db.UserDAO
import com.reus_modernista_front.db.entity.UserEntity
import com.reus_modernista_front.model.user.UserResponse
import com.reus_modernista_front.model.user.addPoints.PointsUser
import com.reus_modernista_front.model.user.addVisitedBuilding.VisitedBuildingUser
import com.reus_modernista_front.model.user.authUser.login.LoginUserRequest
import com.reus_modernista_front.model.user.authUser.register.RegisterUserRequest
import com.reus_modernista_front.model.user.getInfo.GetInfoUserResponse
import com.reus_modernista_front.model.user.getRanking.GetRankingUserResponse
import retrofit2.Response
import retrofit2.awaitResponse

class UserRepository(private val userDAO: UserDAO) {

    var readAllData: LiveData<UserEntity> = userDAO.readDataLoggedUser(Session.getEmailUsuario())

    suspend fun login(loginUser: LoginUserRequest): Response<UserResponse> {
        // Call to API to add new user
        return RetrofitInstance.apiUsers.login(loginUser).awaitResponse()
    }

    suspend fun register(registerUser: RegisterUserRequest): Response<UserResponse> {
        // Call to API to add new user
        return RetrofitInstance.apiUsers.register(registerUser).awaitResponse()
    }

    suspend fun getInfoUser(email: String): Response<GetInfoUserResponse> {
        // Call to API to add new user
        return RetrofitInstance.apiUsers.getInfoUser(email).awaitResponse()
    }

    suspend fun addPoints(pointsUser: PointsUser): Response<UserResponse> {
        return RetrofitInstance.apiUsers.addPoints(pointsUser).awaitResponse()
    }

    suspend fun addVisitedBuilding(visitedBuildingUser: VisitedBuildingUser): Response<UserResponse> {
        return RetrofitInstance.apiUsers.addVisitedBuilding(visitedBuildingUser).awaitResponse()
    }

    suspend fun getAllUsers(): Response<List<GetRankingUserResponse>> {
        return RetrofitInstance.apiUsers.getAllUsers().awaitResponse()
    }

    // Local database actions

    suspend fun addUser(userEntity: UserEntity) {
        //Add to local Data Base
        userDAO.addUser(userEntity)
    }

    suspend fun addLocalPoints(points: Int) {
        //Add to local Data Base
        val currentPoints = userDAO.getPoints(Session.getEmailUsuario())
        userDAO.addPoints(points + currentPoints, Session.getEmailUsuario())
    }

    suspend fun userExists(email: String): Boolean {
        return userDAO.getUserInfoByEmail(email)
    }

    suspend fun userIsLogged(email: String, password: String): Boolean {
        return userDAO.getUserInfo(email, password)
    }

    suspend fun addLocalVisitedBuilding(email: String, visitedBuilding: String) {
        userDAO.addVisitedBuilding(email, visitedBuilding)
    }

    suspend fun getLocalVisitedBuilding(email: String): String {
        return userDAO.getVisitedBuilding(email)
    }

}