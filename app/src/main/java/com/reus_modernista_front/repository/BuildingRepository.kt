package com.reus_modernista_front.repository

import com.reus_modernista_front.api.RetrofitInstance
import com.reus_modernista_front.db.BuildingDAO
import com.reus_modernista_front.db.entity.BuildingEntity
import com.reus_modernista_front.model.building.Building
import retrofit2.Response
import retrofit2.awaitResponse

class BuildingRepository(private val buildingDAO: BuildingDAO) {

    suspend fun getBuildings(): Response<List<Building>> {
        return RetrofitInstance.apiBuildings.getBuildings().awaitResponse()
    }

    suspend fun getLocalBuildings(): MutableList<Building> {
        val resultBuildings: MutableList<Building> = ArrayList()
        val buildings = buildingDAO.getAllBuildings()
        for (building in buildings) {
            val build: Building = Building(
                building._id,
                building.name,
                building.year_of_creation,
                building.architect,
                building.latitude,
                building.longitude,
                building.value
            )
            resultBuildings.add(build)
        }
        return resultBuildings
    }

    suspend fun addBuilding(buildingEntity: BuildingEntity) {
        buildingDAO.addBuilding(buildingEntity)
    }
}