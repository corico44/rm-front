package com.reus_modernista_front.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

object ConnectionCheck {

    fun isConnectedToInternet(applicationContext: Context): Boolean {
        val connectivity: ConnectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivity != null) {
            val info: Array<NetworkInfo> = connectivity.getAllNetworkInfo()
            if (info != null) for (i in info.indices) if (info[i].getState() === NetworkInfo.State.CONNECTED) {
                return true
            }
        }
        return false
    }
}