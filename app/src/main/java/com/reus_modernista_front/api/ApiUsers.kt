package com.reus_modernista_front.api

import com.reus_modernista_front.model.user.UserResponse
import com.reus_modernista_front.model.user.addPoints.PointsUser
import com.reus_modernista_front.model.user.addVisitedBuilding.VisitedBuildingUser
import com.reus_modernista_front.model.user.authUser.login.LoginUserRequest
import com.reus_modernista_front.model.user.authUser.register.RegisterUserRequest
import com.reus_modernista_front.model.user.getInfo.GetInfoUserResponse
import com.reus_modernista_front.model.user.getRanking.GetRankingUserResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiUsers {

    @Headers("Content-Type: application/json")
    @POST("/users/login")
    fun login(
        @Body loginUserRequest: LoginUserRequest
    ): Call<UserResponse>

    @Headers("Content-Type: application/json")
    @POST("/users/register")
    fun register(
        @Body user: RegisterUserRequest
    ): Call<UserResponse>

    @Headers("Content-Type: application/json")
    @GET("/users/{email}")
    fun getInfoUser(
        @Path("email") email: String
    ): Call<GetInfoUserResponse>

    @Headers("Content-Type: application/json")
    @POST("/users/addPoints")
    fun addPoints(
        @Body user: PointsUser
    ): Call<UserResponse>

    @Headers("Content-Type: application/json")
    @POST("/users/addVisitedBuilding")
    fun addVisitedBuilding(
        @Body user: VisitedBuildingUser
    ): Call<UserResponse>

    @Headers("Content-Type: application/json")
    @GET("/users/getRanking")
    fun getAllUsers(): Call<List<GetRankingUserResponse>>
}