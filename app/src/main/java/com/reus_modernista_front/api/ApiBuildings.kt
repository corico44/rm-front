package com.reus_modernista_front.api

import com.reus_modernista_front.model.building.Building
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiBuildings {

    @Headers("Content-Type: application/json")
    @GET("/buildings")
    fun getBuildings(): Call<List<Building>>
}